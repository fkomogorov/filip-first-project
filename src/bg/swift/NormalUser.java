package bg.swift;

import java.time.LocalDateTime;

class NormalUser extends User {
    NormalUser(String userName, String password, LocalDateTime dateTime, String email) {
        super(userName, password, dateTime, email);
    }
}
