package bg.swift;

import java.time.LocalDateTime;


 class Post {

    String content;
    String postAuthor;
    LocalDateTime postDateTime;

    Post(String content, String postAuthor, LocalDateTime postDateTime) {
        this.content = content;
        this.postAuthor = postAuthor;
        this.postDateTime = postDateTime;
    }


}
