package bg.swift;

import bg.swift.Thread.Thread;

import java.time.LocalDateTime;

abstract class User {

    String userName;
    private String password;
    LocalDateTime dateTime;
    String email;

    User(String userName, String password, LocalDateTime dateTime, String email){
        this.userName =userName;
        this.password=password;
        this.dateTime=dateTime;
        this.email=email;
    }

    static String normalUserProcessCommand(String line, ThreadRepository threadRepository) {

        String[] linePortions = line.split(" ");

        String command = linePortions[0].toUpperCase();
        String username = linePortions[1];
        String password = linePortions[2];

        NormalUser credentials;
        boolean commandOutcome;

        switch (command) {
            case "ADD":
                Post post = new Post("Content",
                        "Post Author", LocalDateTime.now());
                threadRepository.add(new Thread("Thread_1",LocalDateTime.now(),post,"Author 1"));
                commandOutcome = true;
                break;
            case "CLOSE":
                String newPassword = linePortions[3];
                credentials = credentialsRepository.find(username);
                commandOutcome = credentials.tryChangePassword(password, newPassword);
                break;

            default:
                return "ERROR!";
        }

        String commandOutcomeString;
        if (commandOutcome) {
            commandOutcomeString = "success";
        } else {
            commandOutcomeString = "fail";
        }

        return String.format("%s %s", command, commandOutcomeString);
    }

}
